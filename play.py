import os
import sys
from env import CardofSpirits
from agent import AgentRand, AgentHuman, AgentTrained
import numpy as np


if __name__ == '__main__':
    argv = sys.argv
    game = CardofSpirits()
    p = [AgentRand(), AgentRand()]
    hide = None
    display = False
    turns = 1000
    all_results = np.array([0, 0, 0])

    for i in range(len(argv)):
        if argv[i] == '-h0':
            if argv[i+1][0] != '-':
                p[0] = AgentHuman(game, argv[i+1])
            else:
                p[0] = AgentHuman(game)
            hide = 1
            display = True
        if argv[i] == '-h1':
            if argv[i+1][0] != '-':
                p[1] = AgentHuman(game, argv[i+1])
            else:
                p[1] = AgentHuman(game)
            hide = 0
            display = True
        if argv[i] == '-a0':
            p[0] = AgentTrained(game, argv[i+1], 0)
        if argv[i] == '-a1':
            p[1] = AgentTrained(game, argv[i+1], 1)
        if argv[i] == '-n':
            turns = int(argv[i+1])
        if argv[i] == '-show':
            display = True

    if display:
        iter = range(turns)
    else:
        from tqdm import tqdm
        iter = tqdm(range(turns), ascii=True)

    for _ in iter:
        game.reset()
        if display:
            print('Trimed cards:', [game.Card_Name[i]
                                    for i in game.cards_trimed])
        for i in range(100):
            if display:                
                game.render(hide=hide)
            if game.result is None:
                player = p[game.turn % 2]
                cid = player.select_cid_best(game.state, game.choices)
                game.use(cid)
            else:
                break
        all_results[1 - game.result] += 1
        if display:            
            print('Finished with:', [
                  'p0-win', 'draw', 'p1-win'][1 - game.result])
            print('All Results:', all_results)
            # os.system("pause")
        else:
            pass

    print('All Results:', all_results)
