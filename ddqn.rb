class Matrix
  attr_reader :row, :col, :data
  def initialize(row, col)
    @row = row
    @col = col
    if block_given?
      @data = Array.new(row * col) {|i|
        yield i / col, i % col
      }
    else
      @data = Array.new(row * col) { 0 }
    end
  end

  def *(m)
    raise [[@row, @col], [m.row, m.col]].to_s if @col != m.row
    Matrix.new(@row, m.col) { |_row, _col|
      @col.times.inject(0){ |s, i|
        s += @data[_row * @col + i] * m.data[_col + m.col * i]
      }
    }
  end

  def +(m)
    raise if @row != m.row || @col != m.col
    Matrix.new(@row, @col) { |_row, _col|
      @data[_row * @col + _col] + m.data[_row * @col + _col]
    }
  end

  def relu!
    @data.each_index {|i| @data[i] = 0 if @data[i] < 0}
  end

  def mean
    @data.inject(0) {|s, i| s += i} / @data.size
  end

  def reshape(row, col)
    row = @row * @col / col if row == -1
    col = @row * @col / row if col == -1
    @row, @col = row, col
    self
  end

  def load_file(file)
    @data = File.read(file).split.collect{|s| s.to_f}
  end
end

class Neural_Network
  def initialize(weights, &block)
    @weights = weights
    @run = block
  end

  def run(m)
    @run.call(m, @weights)
  end  
end

t = Time.now

weights = Marshal.load(File.read("weights.rxdata"))

DDQN = Neural_Network.new(weights) { |v, w|
  a = v
  a = a * w[:w0] + w[:b0]
  a.relu!
  a = a * w[:w1] + w[:b1]
  a.relu!
  a = a * w[:w2] + w[:b2]
  a.relu!
  a = a * w[:w3] + w[:b3]
  a.relu!
  val = a * w[:w4] + w[:b4]
  adv = a * w[:w5] + w[:b5]
  bias = Matrix.new(1, adv.col) { val.data[0] - adv.mean }
  adv + bias
}

p Time.now - t
DDQN.run(Matrix.new(1, 32))
p Time.now - t
