#PBS -N ml-test
#PBS -l nodes=1:ppn=24
#PBS -l Qlist=n24

ulimit -s unlimited

cd $PBS_O_WORKDIR
cp $PBS_NODEFILE node
NCORE=`cat node | wc -l`

date > output.$PBS_JOBID
for ((i=0; i<10; ++i)); do
    echo " * Start Iteration: $i * " >> output.$PBS_JOBID
    bash iteration.sh >> output.$PBS_JOBID
    cp -r best "best.$i"
done

date >> output.$PBS_JOBID