from time import time
import os
import sys
from env import CardofSpirits
from agent import AgentDQN, AgentRand
import numpy as np


if not os.path.exists("agent"):
    os.mkdir("agent")


class Game:
    TrainEpisodes = 10000
    TrainTimes = 40
    TrainEgreedy = 0.50
    PassScore = 0.40
    Output = False

    if len(sys.argv) > 1 and sys.argv[1] == '--init':
        InitAgents = [sys.argv[2], sys.argv[3]]
    else:
        InitAgents = None

    def __init__(self, env):
        self.env = env
        self.agent = AgentDQN(env)
        self.agent.e_greedy = self.TrainEgreedy
        # init agents weights
        if self.InitAgents is not None:
            if os.path.exists(self.InitAgents[0]):
                print('load init agent0: ', self.InitAgents[0])
                self.agent.np2vars('q_target', np.load(self.InitAgents[0]))
                self.agent.np2vars('q_eval', np.load(self.InitAgents[0]))
            if os.path.exists(self.InitAgents[1]):
                print('load init agent1: ', self.InitAgents[1])
                self.agent.np2vars('q_opponent', np.load(self.InitAgents[1]))

        # train which agent first(0) or back(1)
        self.train_pid = 0
        self.train_ver = 0
        self.train_score = 0
        self.scores = {}
        self.agent_rand = AgentRand()

    def run_once(self, rand=1.0):
        self.env.reset()
        if np.random.rand() < rand:
            p = [self.agent.select_cid, self.agent_rand.select_cid]
        else:
            p = [self.agent.select_cid, self.agent.select_cid_opponent]

        for _ in range(100):
            if self.env.result is None:
                play = p[self.env.turn % 2 ^ self.train_pid]
                cid = play(self.env.state, self.env.choices)
                self.env.use_t(cid)
            else:
                break

    def next_train(self, pid, ver):
        # change version only
        if self.train_pid != pid:
            self.agent.run_exchange()
            self.agent.reset_memory()

        self.train_pid = pid
        self.train_ver = ver
        print('* agent%d-v%d *' % (self.train_pid, self.train_ver))
        return self

    def save_weights(self):
        fn = 'agent/agent%d-v%d.npy' % (self.train_pid, self.train_ver)
        q_eval = self.agent.vars2np('q_eval')
        np.save(fn, q_eval)
        print('Weights save to file:', fn)

    def train(self, rand=1.0):
        all_results = np.array([0, 0, 0])
        total_noobs = 0
        print('Training', end='')
        start_time = time()

        for i in range(self.TrainEpisodes):
            self.run_once(rand=rand)
            result = self.env.result
            if self.env.noob is not None:
                total_noobs += 1
            all_results[1 - result] += 1
            data = self.env.train_data(self.train_pid)
            self.agent.learn(data)
            if i % (self.TrainEpisodes // 10) == 0:
                print('.', end='', flush=True)

        finish_time = time()
        print('done.')
        print(" - use %.3fs (for %d episodes)" %
              (finish_time - start_time, self.TrainEpisodes))
        print(" - Win/Draw/Lose: ", all_results / self.TrainEpisodes)

        noob_rate = total_noobs / all_results[2 - 2 * self.train_pid]
        print(" - Noobs: %.3f" % noob_rate)

        self.agent.run_replace()

        return np.hstack((all_results / self.TrainEpisodes, noob_rate))

    def compete(self, p):
        episodes = self.TrainEpisodes // 10
        all_results = np.array([0, 0, 0])
        all_turns = np.array([0, 0, 0])

        print('Evaluation:')
        start_time = time()

        for _ in range(episodes):
            self.env.reset()
            turn = 0
            for turn in range(100):
                if self.env.result is None:
                    player = p[self.env.turn % 2]
                    cid = player.select_cid_best(
                        self.env.state, self.env.choices)
                    self.env.use(cid)
                else:
                    break

            result = self.env.result
            all_results[1 - result] += 1
            all_turns[1 - result] += turn

        finish_time = time()
        print(" - use %.3fs (for %d episodes)" %
              (finish_time - start_time, episodes))
        print(' - Win/Draw/Lose: ', all_results / episodes)

        rates = all_results / episodes

        score = np.sum(rates * [1, 0, -1])
        if self.train_pid == 1:
            score *= -1
        print(' - Score: %.3f' % score, flush=True)
        return score


if __name__ == '__main__':
    g = Game(CardofSpirits())
    c = [[g.agent, g.agent_rand], [g.agent_rand, g.agent]]

    for i in range(g.TrainTimes):
        for pid in range(2):
            g.next_train(pid, i).train(0)
            score = g.compete(c[pid])
            if score >= g.PassScore:
                g.save_weights()
