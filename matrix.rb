class Matrix
  attr_reader :row, :col, :data
  def initialize(row, col)
    @row = row
    @col = col
    if block_given?
      @data = Array.new(row * col) {|i|
        yield i / col, i % col
      }
    else
      @data = Array.new(row * col) { 0 }
    end
  end

  def *(m)
    raise [[@row, @col], [m.row, m.col]].to_s if @col != m.row
    Matrix.new(@row, m.col) { |_row, _col|
      @col.times.inject(0){ |s, i|
        s += @data[_row * @col + i] * m.data[_col + m.col * i]
      }
    }
  end

  def +(m)
    raise if @row != m.row || @col != m.col
    Matrix.new(@row, @col) { |_row, _col|
      @data[_row * @col + _col] + m.data[_row * @col + _col]
    }
  end

  def relu!
    @data.each_index {|i| @data[i] = 0 if @data[i] < 0}
  end

  def mean
    @data.inject(0) {|s, i| s += i} / @data.size
  end

  def reshape(row, col)
    row = @row * @col / col if row == -1
    col = @row * @col / row if col == -1
    @row, @col = row, col
    self
  end

  def load_file(file)
    @data = File.read(file).split.collect{|s| s.to_f}
  end
end

class Neural_Network
  def initialize(weights, &block)
    @weights = weights
    @run = block
  end

  def run(m)
    @run.call(m, @weights)
  end  
end

w0 = Matrix.new(32, 256)
w0.load_file("w0.txt")
b0 = Matrix.new(1, 256)
b0.load_file("b0.txt")

w1 = Matrix.new(256, 256)
w1.load_file("w1.txt")
b1 = Matrix.new(1, 256)
b1.load_file("b1.txt")

w2 = Matrix.new(256, 256)
w2.load_file("w2.txt")
b2 = Matrix.new(1, 256)
b2.load_file("b2.txt")

w3 = Matrix.new(256, 256)
w3.load_file("w3.txt")
b3 = Matrix.new(1, 256)
b3.load_file("b3.txt")

w4 = Matrix.new(256, 1)
w4.load_file("w4.txt")
b4 = Matrix.new(1, 1)
b4.load_file("b4.txt")

w5 = Matrix.new(256, 13)
w5.load_file("w5.txt")
b5 = Matrix.new(1, 13)
b5.load_file("b5.txt")

a = Matrix.new(1, 32)
a = a * w0 + b0
a.relu!
a = a * w1 + b1
a.relu!
a = a * w2 + b2
a.relu!
a = a * w3 + b3
a.relu!
p val = a * w4 + b4
p adv = a * w5 + b5
p adv.mean
bias = Matrix.new(1, 13) { val.data[0] - adv.mean }
p adv + bias

File.open("weights.rxdata", "wb") do |f|
  Marshal.dump({
    w0: w0, w1: w1, w2: w2, w3: w3, w4: w4, w5: w5, 
    b0: b0, b1: b1, b2: b2, b3: b3, b4: b4, b5: b5
  }, f)
end

puts "load"
weights = Marshal.load(File.read("weights.rxdata"))

DDQN = Neural_Network.new(weights) { |v, w|
  a = v
  a = a * w[:w0] + w[:b0]
  a.relu!
  a = a * w[:w1] + w[:b1]
  a.relu!
  a = a * w[:w2] + w[:b2]
  a.relu!
  a = a * w[:w3] + w[:b3]
  a.relu!
  val = a * w[:w4] + w[:b4]
  adv = a * w[:w5] + w[:b5]
  bias = Matrix.new(1, adv.col) { val.data[0] - adv.mean }
  adv + bias
}

t = Time.now
p DDQN.run(Matrix.new(1, 32))
p Time.now - t