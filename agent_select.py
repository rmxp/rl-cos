import os
import time
import sys
import re
import glob
import numpy as np
import pandas as pd
from multiprocessing import Pool
from agent import AgentTrained
from env import CardofSpirits

if not os.path.exists("best"):
    os.mkdir("best")


"""optimize
    make a full compete of all agents and select best of them
    Parallel_Num = 6 (Recomend)
    set Parallel_Num = 8 may cause core dump error
"""
Parallel_Num = 6

Game = CardofSpirits()
Map_Test = []


def load_agents(train_folders):
    agents0, agents1 = [], []
    for folder in train_folders:
        for fn in os.listdir(folder):
            if fn[0: 6] == 'agent0':
                agents0.append(folder + '/' + fn)
            if fn[0: 6] == 'agent1':
                agents1.append(folder + '/' + fn)
    agents0.sort()
    agents1.sort()

    print('Num of Agent0 Candidate:', len(agents0))
    print('Num of Agent1 Candidate:', len(agents1))
    print('Total matches: ', len(agents0) * len(agents1))
    return [agents0, agents1]


def full_compete(agents0, agents1):
    index = 0
    total_matches = len(agents0) * len(agents1)
    all_result = []
    ret = []
    for _ in range(len(agents0)):
        Map_Test.append([0] * len(agents1))

    pool = Pool(Parallel_Num)
    for agent0 in agents0:
        for agent1 in agents1:
            score = pool.apply_async(single_compete_task,
                                     (agent0, agent1, index % Parallel_Num))
            ret.append(score)

            if index % Parallel_Num == Parallel_Num - 1:
                print(' - Cal: %d / %d' %
                      (index // Parallel_Num, total_matches // Parallel_Num))
                pool.close()
                pool.join()
                for r in ret:
                    all_result.append(r.get())
                ret.clear()

                pool = Pool(Parallel_Num)

            index += 1

    pool.close()
    pool.join()
    for r in ret:
        all_result.append(r.get())
    ret.clear()

    index = 0
    for i in range(len(agents0)):
        for j in range(len(agents1)):
            Map_Test[i][j] = all_result[index]
            index += 1


def single_compete_task(agent0, agent1, index, episodes=1000):
    agents = [
        AgentTrained(Game, agent0, a_id=index * 2),
        AgentTrained(Game, agent1, a_id=index * 2 + 1),
    ]
    all_results = np.array([0, 0, 0])
    for _e in range(episodes):
        Game.reset()
        for _ in range(100):
            if Game.result is None:
                player = agents[Game.turn % 2]
                cid = player.select_cid_best(
                    Game.state, Game.choices)
                Game.use(cid)
            else:
                break

        all_results[1 - Game.result] += 1

    return np.sum(all_results * [1, 0, -1]) / episodes


def single_compete_task_fake(agent0, agent1, index, episodes=1000):
    return np.random.rand() - 0.5


def save_map(agents0, agents1):
    map_test_np = np.array(Map_Test)
    np.save('map.npy', map_test_np)
    df = pd.DataFrame(map_test_np, index=agents0, columns=agents1)
    df.to_csv('map.csv')


def sort_agents(alpha=0.5):
    scores = np.array(Map_Test)
    x = np.sum(scores, axis=1) / scores.shape[0]
    y = np.sum(0 - scores, axis=0) / scores.shape[1]

    x_order = np.argsort(x)
    y_order = np.argsort(y)

    for i in range(20):
        print('sort', i, x_order, y_order)
        k = alpha / np.std(y)
        x = np.dot(scores, np.exp(k*y)) / np.sum(np.exp(k*y))
        k = alpha / np.std(x)
        y = np.dot(-scores.T, np.exp(k*x)) / np.sum(np.exp(k*x))

        if np.array_equal(x_order, np.argsort(x)) and np.array_equal(y_order, np.argsort(y)):
            break
        else:
            x_order = np.argsort(x)
            y_order = np.argsort(y)

    return [x_order[::-1], y_order[::-1]]


def load_best(train_folders, num=64):
    agents_path = [[], []]
    agents_score = [[], []]
    pid = 0

    for folder in train_folders:
        with open(folder + '/result.txt', "r") as f:
            for line in f.readlines():
                line = line.strip()
                m = re.match("\* (.+) \*", line)
                if m:
                    agents_path[pid].append(
                        folder + '/agent/' + m.group(1) + '.npy')

                m = re.match("\- Score: (.+)", line)
                if m:
                    agents_score[pid].append(float(m.group(1)))
                    pid = 1 - pid

    best = [[], []]

    if os.path.exists('best/agent0.npy'):
        best[0].append('best/agent0.npy')
    if os.path.exists('best/agent1.npy'):
        best[1].append('best/agent1.npy')

    for i in [0, 1]:
        for j in np.argsort(agents_score[i])[::-1][0:num]:
            if os.path.exists(agents_path[i][j]):
                best[i].append(agents_path[i][j])

    return best


if __name__ == '__main__':
    if len(sys.argv) > 1:
        train_folders = glob.glob(sys.argv[1])
    else:
        train_folders = '.'

    if len(sys.argv) > 2:
        num = int(sys.argv[2])
    else:
        num = 64

    agents0, agents1 = load_best(train_folders, num=num)

    t = time.time()
    full_compete(agents0, agents1)
    print('Cost time:', time.time() - t)

    save_map(agents0, agents1)

    x_order, y_order = sort_agents()

    print('cp best agents to best.')

    for i in range(8):
        print(i, agents0[x_order[i]], agents1[y_order[i]])
        os.system('cp %s best/agent0-v%d.npy' % (agents0[x_order[i]], i))
        os.system('cp %s best/agent1-v%d.npy' % (agents1[y_order[i]], i))
