import numpy as np
import tensorflow as tf


# a DQN frame work

class DoubleDQN:
    def __init__(self):
        self.n_layer_input = 30
        self.n_layer_hidden = 256
        self.n_layer_output = 12
        self.n_depth_hidden = 3
        self.reward_decay = 0.8
        self.learning_rate = 0.0001
        self._build_net()

    def _build_net(self):
        self.s = tf.placeholder(
            tf.float32, [None, self.n_layer_input], name='s')
        self.a = tf.placeholder(tf.int32, [None, ], name='a')
        self.q = tf.placeholder(tf.float32, [None, ], name='q')

        self.new = tf.placeholder(tf.float32, shape=[1, None])

        # we have 2 q-networks:
        self.q_eval = self._build_net_single('q_eval')
        self.q_target = self._build_net_single('q_target')
        self.q_opponent = self._build_net_single('q_opponent')

        with tf.variable_scope('train'):
            a_indices = tf.stack(
                [tf.range(tf.shape(self.a)[0], dtype=tf.int32), self.a], axis=1)
            q_eval = tf.gather_nd(params=self.q_eval, indices=a_indices)

            self.loss = tf.reduce_mean(
                tf.squared_difference(q_eval, self.q), name='TD_error')
            lr = self.learning_rate
            self.train_op = tf.train.RMSPropOptimizer(lr).minimize(self.loss)

        t_params = tf.get_collection(
            tf.GraphKeys.GLOBAL_VARIABLES, scope='q_target')
        e_params = tf.get_collection(
            tf.GraphKeys.GLOBAL_VARIABLES, scope='q_eval')
        o_params = tf.get_collection(
            tf.GraphKeys.GLOBAL_VARIABLES, scope='q_opponent')

        with tf.variable_scope('soft_replacement'):
            # e=e, t=t -> e=e, [t=e]
            self.replace_op = [tf.assign(t, e)
                               for t, e in zip(t_params, e_params)]
            # e=e, t=t, o=o -> e=e, t=t, [o=e]
            self.saveoppt_op = [tf.assign(o, e)
                                for o, e in zip(o_params, e_params)]
            # from e=e, t=t, o=o -> e=o, t=o, o=e
            # t=o: e, o, o
            # o=e: e, o, e
            # e=t: o, o, e
            self.exchange_op = [tf.assign(t, o) for o, t in zip(o_params, t_params)] + [tf.assign(
                o, e) for o, e in zip(o_params, e_params)] + [tf.assign(e, t) for e, t in zip(e_params, t_params)]

        self.sess = tf.Session()
        self.sess.run(tf.global_variables_initializer())

    def _build_net_single(self, scope):
        with tf.variable_scope(scope):
            i = 1
            _w = tf.get_variable(
                'w%d' % i, [self.n_layer_input, self.n_layer_hidden])
            _b = tf.get_variable('b%d' % i, [1, self.n_layer_hidden])
            _z = tf.add(tf.matmul(self.s, _w), _b)
            _a = tf.nn.relu(_z)

            for _ in range(self.n_depth_hidden - 1):
                i += 1
                _w = tf.get_variable(
                    'w%d' % i, [self.n_layer_hidden, self.n_layer_hidden])
                _b = tf.get_variable('b%d' % i, [1, self.n_layer_hidden])
                _z = tf.add(tf.matmul(_a, _w), _b)
                _a = tf.nn.relu(_z)

            i += 1
            _w = tf.get_variable(
                'w%d' % i, [self.n_layer_hidden, self.n_layer_output])
            _b = tf.get_variable('b%d' % i, [1, self.n_layer_output])
            _z = tf.add(tf.matmul(_a, _w), _b)
            _a = tf.nn.relu(_z)

            return _a

    def vars2np(self, scope):
        list = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=scope)
        list_v = self.sess.run(list)
        return np.hstack([v.reshape(1, -1) for v in list_v])

    def np2vars(self, scope, value):
        list = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=scope)
        i = 0
        for key in list:
            shape = self.sess.run(tf.shape(key))
            num = shape[0]*shape[1]
            self.sess.run(tf.assign(key, tf.reshape(
                self.new[0, i:i+num], shape)), feed_dict={self.new: value})
            i += num

    def run_replace(self):
        self.sess.run(self.replace_op)

    def run_exchange(self):
        self.sess.run(self.exchange_op)

    def run_saveoppt(self):
        self.sess.run(self.saveoppt_op)

    def run_eval(self, state):
        return self.sess.run(self.q_eval, feed_dict={self.s: state})

    def run_target(self, state):
        return self.sess.run(self.q_target, feed_dict={self.s: state})

    def run_opponent(self, state):
        return self.sess.run(self.q_opponent, feed_dict={self.s: state})

    def run_train(self, train_data):
        m = train_data.shape[0]
        gamma = self.reward_decay

        state = train_data[:, :self.n_layer_input]
        action = train_data[:, self.n_layer_input]
        reward = train_data[:, self.n_layer_input + 1]
        done = train_data[:, self.n_layer_input + 2]
        next_state = train_data[:, -self.n_layer_input:]

        # double dqn:
        values_next = self.run_eval(next_state)
        # values_next = self.run_target(next_state)
        action_next = np.argmax(values_next, axis=1).reshape(
            1, -1)  # max index of each line
        target_values = self.run_target(next_state)

        q_target = reward + (1 - done) * gamma * \
            target_values[range(m), action_next].reshape(-1, )

        # now we get reward, action and state
        _, loss = self.sess.run([self.train_op, self.loss], feed_dict={
            self.s: state, self.a: action, self.q: q_target})
        return loss


class DuelingDQN(DoubleDQN):
    """Dueling DQN
    Split the Q value into 2 parts before return: Q(s, a) = V(s) + A(s, a)
    """

    def _build_net_single(self, scope):
        with tf.variable_scope(scope):
            i = 1
            _w = tf.get_variable(
                'w%d' % i, [self.n_layer_input, self.n_layer_hidden])
            _b = tf.get_variable('b%d' % i, [1, self.n_layer_hidden])
            _z = tf.add(tf.matmul(self.s, _w), _b)
            _a = tf.nn.relu(_z)

            for _ in range(self.n_depth_hidden - 1):
                i += 1
                _w = tf.get_variable(
                    'w%d' % i, [self.n_layer_hidden, self.n_layer_hidden])
                _b = tf.get_variable('b%d' % i, [1, self.n_layer_hidden])
                _z = tf.add(tf.matmul(_a, _w), _b)
                _a = tf.nn.relu(_z)

            i += 1
            # value V(s)
            _w = tf.get_variable(
                'w%d' % i, [self.n_layer_hidden, 1])
            _b = tf.get_variable('b%d' % i, [1, 1])
            _z = tf.add(tf.matmul(_a, _w), _b)
            _val = _z

            i += 1
            # advantage A(s,a)
            _w = tf.get_variable(
                'w%d' % i, [self.n_layer_hidden, self.n_layer_output])
            _b = tf.get_variable('b%d' % i, [1, self.n_layer_output])
            _z = tf.add(tf.matmul(_a, _w), _b)
            _adv = _z

            # Q(s, a) = V(s) + A(s, a) - mean(A(s, a))
            _a = _val + _adv - tf.reduce_mean(_adv, axis=1, keepdims=True)

            return _a
